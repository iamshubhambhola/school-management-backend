const { pick } = require("lodash");

function getFields(data) {
  return pick(data, [
    "fieldname",
    "originalname",
    "mimetype",
    "bucket",
    "key",
    "location",
  ]);
}

module.exports = {
  getFields,
};
