const Joi = require("joi");

module.exports = {
  getAttendance: Joi.object().keys({
    date: Joi.string().isoDate().required().trim(),
  }),
  createAttendance: Joi.object().keys({
    date: Joi.string().isoDate().trim().required(),
    time: Joi.object()
      .keys({
        start: Joi.number().required(),
        end: Joi.number().required(),
      })
      .required(),
    subject: Joi.string().trim().required(),
    student_absentees: Joi.array(),
  }),
};
