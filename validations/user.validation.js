const Joi = require("joi");
const { uploadFile } = require("./uploadFile.validation");

module.exports = {
  selectUserRole: Joi.object().keys({
    name: Joi.string().required().trim(),
    role: Joi.string()
      .required()
      .valid("PRINCIPAL", "TEACHER", "PARENT", "STUDENT"),
    image: uploadFile,
  }),
};
