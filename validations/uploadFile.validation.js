const Joi = require("joi");

module.exports = {
  uploadFile: Joi.object().keys({
    fieldname: Joi.string().required(),
    originalname: Joi.string().required(),
    mimetype: Joi.string().required(),
    bucket: Joi.string().required(),
    key: Joi.string().required(),
    location: Joi.string().required(),
  }),
};
