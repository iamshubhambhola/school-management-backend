const { Router } = require("express");
const { checkAuth } = require("../../middlewares/authorization");
const controller = require("./principal.controller");
const router = Router();

router
  .route("/get_join_institute_requests")
  .get(checkAuth, controller.getJoinInstituteRequests);
router
  .route("/approve_join_institute_request")
  .post(checkAuth, controller.approveJoinInstituteRequest);

module.exports = router;
