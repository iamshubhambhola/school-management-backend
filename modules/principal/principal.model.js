const mongoose = require("mongoose");

const transformFields = ["id", "userID", "name", "createdAt", "updatedAt"];

const PrincipalSchema = new mongoose.Schema(
  {
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    name: {
      type: String,
    },
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

function autoPopulate(next) {
  this.populate("userID");
  next();
}
PrincipalSchema.pre("find", autoPopulate);
PrincipalSchema.pre("findOne", autoPopulate);

module.exports = mongoose.model("Principal", PrincipalSchema);
