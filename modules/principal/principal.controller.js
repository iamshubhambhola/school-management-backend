const { roles } = require("../../config/roles");
const { types } = require("../../config/requestType");
const { sendSuccess, sendError } = require("../../utils/response");
const Teacher = require("../teacher/teacher.model");
const JoinRequest = require("../JoinRequest/JoinRequest.model");
const mongoose = require("mongoose");

class Controller {
  async getJoinInstituteRequests(req, res, next) {
    const { role: userRole } = req.user;

    if (userRole !== roles[1]) {
      return sendError(next, "User has no access", 401);
    }

    const { instituteID } = req.query;

    let requests = await JoinRequest.find({ for: instituteID }).populate("by");
    return sendSuccess(res, requests);
  }

  async approveJoinInstituteRequest(req, res, next) {
    const { role: userRole } = req.user;

    if (userRole !== roles[1]) {
      return sendError(next, "User has no access", 401);
    }

    const { requestID } = req.body;

    JoinRequest.find({ _id: requestID })
      .then((joinRequest) => {
        if (joinRequest[0].type !== types[0])
          sendError(next, "Request of Classroom!");
        else {
          Teacher.updateOne(
            { userID: mongoose.Types.ObjectId(joinRequest[0].by) },
            {
              $set: { institute: mongoose.Types.ObjectId(joinRequest[0].for) },
            },
            function (err, result) {
              if (err) {
                return sendError(next, err);
              }
              return sendSuccess(res, result);
            }
          );
          JoinRequest.deleteOne({ _id: requestID }).then(() => {
            console.log("request deleted");
          });
        }
      })
      .catch((err) => sendError(next, err));
  }
}

module.exports = new Controller();
