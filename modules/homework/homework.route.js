const { Router } = require("express");
const { checkAuth } = require("../../middlewares/authorization");
const controller = require("./homework.controller");
const { s3Upload } = require("../../middlewares/upload");
const router = Router();

router.route("/create_homework").post(checkAuth, controller.createHomework);

router
  .route("/create_homework_pdf")
  .post(checkAuth, s3Upload.single("file"), controller.createHomeworkPdf);

router.route("/update_homework/:id").put(checkAuth, controller.updateHomework);

router
  .route("/update_homework_pdf/:id")
  .put(checkAuth, s3Upload.single("file"), controller.updateHomeworkPdf);

router
  .route("/homework_created_by_teacher")
  .get(checkAuth, controller.homeworkCreatedByTeacher);

router.route("/get_questions/:id").get(controller.getQuestions);

router.route("/:id").get(checkAuth, controller.findById);

module.exports = router;
