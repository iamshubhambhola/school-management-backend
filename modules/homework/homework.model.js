const mongoose = require("mongoose");
const { jsonTransform } = require("../../utils/db");
const UploadFileSchema = require("../common/uploadFile.schema");

const transformFields = [
  "id",
  "completed",
  "type",
  "teacher",
  "deadline",
  "name",
  "attachment",
  "createdAt",
  "updatedAt",
];
const HomeworkSchema = new mongoose.Schema(
  {
    completed: {
      type: Boolean,
      default: false,
    },
    type: {
      type: String,
      enum: ["PDF", "MCQ"],
    },
    deadline: {
      type: Date,
    },
    attachment: {
      type: UploadFileSchema,
    },
    teacher: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Teacher",
    },
    numberOfQues: {
      type: Number,
    },
    name: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
    toJSON: {
      transform: (doc) => jsonTransform(doc, transformFields),
    },
  }
);

module.exports = mongoose.model("Homework", HomeworkSchema);
