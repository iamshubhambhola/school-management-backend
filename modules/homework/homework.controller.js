const { roles } = require("../../config/roles");
const { sendSuccess, sendError } = require("../../utils/response");
const Homework = require("./homework.model");
const Question = require("../question/question.model");
const { getFields } = require("../../utils/upload");

/*
//use by upload form
app.post('/upload', upload.array('upl', 25), function (req, res, next) {
    res.send({
        message: "Uploaded!",
        urls: req.files.map(function(file) {
            return {url: file.location, name: file.key, type: file.mimetype, size: file.size};
        })
    });
});
*/
class Controller {
  async createHomework(req, res, next) {
    const { _id: userID, role: userRole } = req.user;
    if (
      userRole === roles[0] ||
      userRole === roles[4] ||
      userRole === roles[3]
    ) {
      return sendError(next, "User has no access", 401);
    }

    const { questions: questions, name, deadline } = req.body;

    const newHomework = new Homework({
      completed: req.body.completed,
      teacher: userID,
      name,
      type: "MCQ",
      deadline,
      numberOfQues: questions ? questions.length : 0,
    });
    newHomework
      .save()
      .then((homework) => {
        questions.forEach((ques) => {
          ques.homework = homework._id;
        });
        Question.create(questions)
          .then(() => {
            return sendSuccess(res, homework);
          })
          .catch((err) => {
            return sendError(next, err);
          });
      })
      .catch((err) => sendError(next, err));
  }

  async createHomeworkPdf(req, res, next) {
    const { _id: userID, role: userRole } = req.user;
    if (
      userRole === roles[0] ||
      userRole === roles[4] ||
      userRole === roles[3]
    ) {
      return sendError(next, "User has no access", 401);
    }

    const { name, deadline, completed } = req.body;

    const newHomework = new Homework({
      completed: completed,
      teacher: userID,
      name: name,
      type: "PDF",
      deadline: deadline,
      attachment: getFields(req.file),
    });
    newHomework
      .save()
      .then((homework) => {
        return sendSuccess(res, homework);
      })
      .catch((err) => sendError(next, err));
  }

  async updateHomeworkPdf(req, res, next) {
    const { role: userRole } = req.user;
    if (
      userRole === roles[0] ||
      userRole === roles[4] ||
      userRole === roles[3]
    ) {
      return sendError(next, "User has no access", 401);
    }

    Homework.updateOne(
      { _id: req.params.id },
      {
        $set: req.body,
      }
    )
      .then((homework) => {
        sendSuccess(res, homework);
      })
      .catch((err) => {
        return sendError(next, err);
      });
  }

  async updateHomework(req, res, next) {
    const { role: userRole } = req.user;
    if (
      userRole === roles[0] ||
      userRole === roles[4] ||
      userRole === roles[3]
    ) {
      return sendError(next, "User has no access", 401);
    }

    const { questions: questions, name, deadline } = req.body;

    Homework.updateOne(
      { _id: req.params.id },
      {
        $set: {
          type: "MCQ",
          completed: req.body.completed,
          numberOfQues: questions ? questions.length : 0,
          name,
          deadline,
        },
      }
    )
      .then((homework) => {
        Question.deleteMany({ homework: req.params.id })
          .then(() => {
            questions.forEach((ques) => {
              ques.homework = req.params.id;
            });
            Question.create(questions)
              .then(() => {
                return sendSuccess(res, homework);
              })
              .catch((err) => {
                return sendError(next, err);
              });
          })
          .catch((err) => {
            return sendError(next, err);
          });
      })
      .catch((err) => {
        return sendError(next, err);
      });
  }

  async homeworkCreatedByTeacher(req, res, next) {
    const { _id: userID, role: userRole } = req.user;
    if (
      userRole === roles[0] ||
      userRole === roles[4] ||
      userRole === roles[3]
    ) {
      return sendError(next, "User has no access", 401);
    }
    try {
      const homeworks = await Homework.find({
        teacher: userID,
      });
      return sendSuccess(res, homeworks);
    } catch (error) {
      return sendError(next, error);
    }
  }

  async findById(req, res, next) {
    const { id: homeworkID } = req.params;
    let homework;
    try {
      homework = await Homework.findById(homeworkID).exec();
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, homework);
  }

  async getQuestions(req, res, next) {
    try {
      const questions = await Question.find({ homework: req.params.id });
      return sendSuccess(res, questions);
    } catch (error) {
      return sendError(next, error);
    }
  }
}

module.exports = new Controller();
