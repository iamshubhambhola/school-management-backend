const joi = require("joi");

module.exports = {
  getAttendance: joi.object().keys({
    date: joi.string().isoDate().required().trim(),
  }),
  createAttendance: joi.object().keys({
    date: joi.string().isoDate().trim().required(),
    time: joi
      .object()
      .keys({
        start: joi.number().required(),
        end: joi.number().required(),
      })
      .required(),
    subject: joi.string().trim().required(),
    student_absentees: joi.array(),
  }),
};
