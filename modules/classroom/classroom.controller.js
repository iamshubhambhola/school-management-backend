const { roles } = require("../../config/roles");
const { sendSuccess, sendError } = require("../../utils/response");
const Teacher = require("../teacher/teacher.model");
const Timetable = require("../timetable/timetable.model");
const Classroom = require("./classroom.model");
const Attendance = require("../attendance/attendance.model");
const JoinRequest = require("../JoinRequest/JoinRequest.model");
const { types } = require("../../config/requestType");
const Student = require("../student/student.model");
class Controller {
  async createClassroom(req, res, next) {
    const { _id: userID, role: userRole } = req.user;
    if (
      userRole === roles[0] ||
      userRole === roles[4] ||
      userRole === roles[3]
    ) {
      return sendError(next, "User has no access", 401);
    }

    const teacher = await Teacher.findOne({ userID }).lean().exec();

    const { grade: c_grade, div: c_div } = req.body;

    const classroomExists = await Classroom.findOne({
      grade: c_grade,
      div: c_div,
      institute: teacher.institute,
    })
      .lean()
      .exec();

    if (classroomExists) {
      return sendError(next, "Classroom already exists", 400);
    }

    const newClassroom = new Classroom({
      grade: c_grade,
      div: c_div,
      teacher: userID,
      institute: teacher.institute,
      class_code: Classroom.generateClassCode(),
    });
    newClassroom
      .save()
      .then((classroom) => sendSuccess(res, classroom))
      .catch((err) => sendError(next, err));
  }

  async addUpdateSubjects(req, res, next) {
    const { id: classroomID } = req.params;
    const { subjects } = req.body;

    let updatedClassroom;
    try {
      updatedClassroom = await Classroom.findByIdAndUpdate(
        classroomID,
        {
          subjects,
        },
        { new: true }
      );
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, updatedClassroom);
  }

  async getSubjects(req, res) {
    const { id: classroomID } = req.params;

    const subjects = (await Classroom.findById(classroomID).lean().exec())
      .subjects;

    return sendSuccess(res, subjects);
  }

  async getTimetable(req, res, next) {
    const { id: classroomID } = req.params;

    const classroom = await Classroom.findById(classroomID).lean().exec();
    if (!classroom) {
      return sendError(next, "Invalid classroom ID", 400);
    }
    if (!classroom.timetable) {
      return sendSuccess(res, []);
    }

    const timetable = await Timetable.findById(classroom.timetable).populate(
      "days.slots.teacher"
    );
    return sendSuccess(res, timetable);
  }

  async createUpdateTimetable(req, res, next) {
    const { id: classroomID } = req.params;
    const { timetable } = req.body;

    // const classroom = await Classroom.findById(classroomID).exec();
    // // if(classroom.teacher !== req.user._id){
    // //   return sendError(next, "Unauthorized", 401);
    // // }

    const classroom = await Classroom.findById(classroomID);
    if (!classroom) {
      return sendError(next, "Invalid classroom ID");
    }

    let updated = false;
    let entity;
    if (classroom.timetable) {
      try {
        entity = await Timetable.findByIdAndUpdate(
          classroom.timetable,
          { days: timetable },
          { new: true }
        ).exec();
      } catch (err) {
        return next(err);
      }

      updated = true;
    } else {
      try {
        entity = await Timetable.create({
          days: timetable,
        });
      } catch (err) {
        return next(err);
      }
    }

    const subjectTeachers = [];

    let updatedClassroom;
    try {
      updatedClassroom = await Classroom.findByIdAndUpdate(
        classroomID,
        {
          timetable: entity._id,
          subject_teachers: subjectTeachers,
        },
        { new: true }
      );
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, {
      classroom: updatedClassroom,
      timetable: entity,
      updated,
    });
  }

  async updateTimetable(req, res, next) {
    const { id: classroomID } = req.params;
    const { timetable } = req.body;

    // const classroom = await Classroom.findById(classroomID).exec();
    // // if(classroom.teacher !== req.user._id){
    // //   return sendError(next, "Unauthorized", 401);
    // // }

    const classroom = await Classroom.findById(classroomID).lean().exec();
    if (!classroom.timetable) {
      return sendError(next, "Classroom does not have a timetable", 400);
    }

    let updatedTimetable;
    try {
      updatedTimetable = await Timetable.findByIdAndUpdate(
        classroom.timetable,
        { days: timetable },
        { new: true }
      ).exec();
    } catch (err) {
      return next(err);
    }

    const subjectTeachers = [];
    try {
      await Classroom.updateOne(
        { _id: classroomID },
        {
          subject_teachers: subjectTeachers,
        }
      );
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, updatedTimetable);
  }

  async deleteTimetable(req, res, next) {
    const { id: classroomID } = req.params;

    const classroom = await Classroom.findById(classroomID).lean().exec();
    if (!classroom) {
      return sendError(next, "Invalid classroom ID");
    }
    try {
      await Timetable.findByIdAndDelete(classroom.timetable);
    } catch (err) {
      return next(err);
    }

    let updatedClassroom;
    try {
      updatedClassroom = await Classroom.findByIdAndUpdate(
        classroomID,
        {
          timetable: null,
        },
        { new: true }
      );
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, updatedClassroom);
  }

  async getAttendance(req, res, next) {
    const { id: classroomID } = req.params;
    const { date } = req.query;

    let classroom;
    try {
      classroom = await Classroom.findById(classroomID)
        .select("timetable")
        .lean()
        .exec();
      if (!classroom) {
        return sendError(next, "Classroom does not exist", 400);
      }
    } catch (err) {
      return next(err);
    }

    let targetTimetable;
    try {
      targetTimetable = await Timetable.findById(classroom.timetable)
        .lean()
        .exec();
      if (!targetTimetable) {
        return sendSuccess(res, []);
      }
    } catch (err) {
      return next(err);
    }

    const day = new Date(date).getDay();
    const slots = targetTimetable.days[day] && targetTimetable.days[day].slots;
    if (!slots) {
      return sendSuccess(res, []);
    }

    const attendance = await Attendance.find({ date, classroom: classroomID })
      .select("time")
      .lean()
      .exec();

    const attendanceSet = new Set();
    attendance.forEach((slot) => {
      const time = `${slot.time.start}-${slot.time.end}`;
      attendanceSet.add(time);
    });

    slots.forEach((slot) => {
      const time = `${slot.time.start}-${slot.time.end}`;
      if (attendanceSet.has(time)) {
        slot.marked = true;
      } else {
        slot.marked = false;
      }
    });

    return sendSuccess(res, {
      date,
      slots,
    });
  }

  async createAttendance(req, res, next) {
    const { date, time, student_absentees, subject } = req.body;
    const { _id: userID } = req.user;
    const { id: classroomID } = req.params;

    let classroom;
    try {
      classroom = await Classroom.findById(classroomID)
        .select("_id")
        .lean()
        .exec();
      if (!classroom) {
        return sendError(next, "Invalid classroom ID");
      }
    } catch (err) {
      return next(err);
    }

    let newAttendance;
    try {
      newAttendance = await Attendance.create({
        date,
        time,
        student_absentees,
        subject,
        classroom: classroom._id,
        teacher: userID,
      });
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, newAttendance);
  }

  async joinClassroomRequest(req, res, next) {
    const { _id: userID } = req.user;
    const { code: classCode } = req.params;

    const classroom = await Classroom.findOne({ class_code: classCode })
      .select("_id")
      .lean()
      .exec();
    if (!classroom) {
      return sendError(next, "Invalid class code");
    }

    let newJoinRequest;
    try {
      newJoinRequest = await JoinRequest.create({
        type: types[1],
        for: classroom._id,
        by: userID,
      });
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, newJoinRequest);
  }

  async getClassroomJoinRequests(req, res, next) {
    const { id: classroomID } = req.params;

    let requests;
    try {
      requests = await JoinRequest.find({ for: classroomID })
        .populate("by")
        .exec();
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, requests);
  }

  async approveClassroomRequests(req, res, next) {
    const { ids: requestIDs } = req.body;
    const { id: classroomID } = req.params;

    let requests;
    try {
      requests = await JoinRequest.find({ _id: { $in: requestIDs } });
    } catch (err) {
      return next(err);
    }

    const studentIDs = [];
    requests.forEach((request) => studentIDs.push(request.by));

    try {
      await Student.updateMany(
        { _id: { $in: studentIDs } },
        { $addToSet: { classrooms: classroomID } }
      );
    } catch (err) {
      return next(err);
    }

    try {
      await JoinRequest.deleteMany({ _id: { $in: requestIDs } });
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, { message: "Requests approved" });
  }

  async getStudents(req, res, next) {
    const { id: classroomID } = req.params;

    let students;
    try {
      students = await Student.find({
        classrooms: classroomID,
      }).populate("userID");
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, students);
  }
}

module.exports = new Controller();
