const mongoose = require("mongoose");

const transformFields = [
  "id",
  "class_code",
  "grade",
  "div",
  "subject",
  "teacher",
  "institute",
  "timetable",
  "subject_teachers",
  "subjects",
  "createdAt",
  "updatedAt",
];

const ClassroomSchema = new mongoose.Schema(
  {
    grade: {
      type: String,
      required: true,
    },
    div: {
      type: String,
      required: true,
    },
    teacher: {
      type: mongoose.Schema.Types.ObjectID,
      ref: "Teacher",
      required: true,
    },
    institute: {
      type: mongoose.Schema.Types.ObjectID,
      ref: "Institute",
      required: true,
    },
    subjects: {
      type: [String],
      default: [],
    },
    class_code: {
      type: String,
      required: true,
    },
    subject_teachers: [
      {
        type: mongoose.Schema.Types.ObjectID,
        ref: "Teacher",
      },
    ],
    timetable: {
      type: mongoose.Schema.Types.ObjectID,
      ref: "Timetable",
    },
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

ClassroomSchema.statics.generateClassCode = function () {
  return Math.random().toString(36).slice(2, 10);
};

module.exports = mongoose.model("Classroom", ClassroomSchema);
