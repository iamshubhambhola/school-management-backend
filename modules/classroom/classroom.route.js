const { Router } = require("express");
const { checkAuth } = require("../../middlewares/authorization");
const requestValidator = require("../../middlewares/validator");
const controller = require("./classroom.controller");
const validation = require("../../validations/classroom.validation");
const router = Router();

router.route("/create_classroom").post(checkAuth, controller.createClassroom);

router
  .route("/:id/subjects")
  .get(checkAuth, controller.getSubjects)
  .put(checkAuth, controller.addUpdateSubjects);

router.route("/:id/get_timetable").get(checkAuth, controller.getTimetable);

router
  .route("/:id/set_timetable")
  .put(checkAuth, controller.createUpdateTimetable);

router
  .route("/:id/delete_timetable")
  .put(checkAuth, controller.deleteTimetable);

router
  .route("/:id/attendance")
  .get(
    checkAuth,
    requestValidator(validation.getAttendance, "query"),
    controller.getAttendance
  )
  .post(
    checkAuth,
    requestValidator(validation.createAttendance),
    controller.createAttendance
  );

//create join requests routes

router.route("/:id/students").get(controller.getStudents);

module.exports = router;
