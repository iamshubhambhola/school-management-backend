const { roles } = require("../../config/roles");
const { getUserModel } = require("../../utils/db");
const { sendSuccess, sendError } = require("../../utils/response");
const UserModel = require("../user/user.model"); 
const Announcement = require("./announcement.model");
const Classroom = require("../classroom/classroom.model");

class Controller {  

    async createAnnouncement(req, res, next) { 
      const { _id: userID, role: userRole  } = req.user;     
      if (userRole!== roles[1]) {
        return sendError(next, "User has no access", 401);
      }   
      
      
      const newAnnouncement = new Announcement({
        title : req.body.title,
        desc : req.body.desc, 
        target : req.body.target, 
        institute : req.body.institute_id,
        createdBy: userID
      });
      newAnnouncement.save()
      .then( announcement => sendSuccess(res,announcement))
      .catch(err => sendError(next,err)); 
    }

    async getAnnouncements(req, res, next) { 
      const { _id: userID } = req.user;
      const currentUser = await UserModel.findById(userID);
      
      if (currentUser.role !== roles[1]) {
        return sendError(next, "User has no access", 401);
      }  
      
      Announcement.find({ createdBy: userID})
          .sort({ createdAt: -1 }) 
          .then((announcements) => {       
            sendSuccess(res,announcements);
          }).catch(err => sendError(next,err)); 
    } 
  }

module.exports = new Controller();

