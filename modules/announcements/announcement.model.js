const mongoose = require("mongoose");

const transformFields = [
  "id",
  "title",
  "desc",
  "target",
  "institute",
  "classroom",
  "createdBy",
  "createdAt",
  "updatedAt",
];

const AnnouncementSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: "Title is required",
    },
    desc: {
      type: String, 
    },
    target: {
      type: String,
      enum: ["INSTITUTE","TEACHERS","STUDENTS","PARENTS"],
      default: "INSTITUTE" 
    },
    institute: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Institute",
      /*type: String,*/
    },
    classroom: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Classroom",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

module.exports = mongoose.model("Announcement", AnnouncementSchema);
