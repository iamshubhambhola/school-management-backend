const { Router } = require("express");
const { checkAuth } = require("../../middlewares/authorization");
const controller = require("./announcement.controller");
const router = Router();
 
router
  .route("/create_announcement")
  .post(checkAuth, controller.createAnnouncement);
router
  .route("/get_announcements")
  .get(checkAuth, controller.getAnnouncements);


module.exports = router;
