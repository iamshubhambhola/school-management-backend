const mongoose = require("mongoose");
const AttendanceSlotSchema = new mongoose.Schema(
  {
    day: {
      type: Number,
      min: 0,
      max: 6,
      required: true,
    },
    time: {
      start: {
        type: Number,
        required: true,
      },
      end: {
        type: Number,
        required: true,
      },
    },
    class: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Classroom",
      required: true,
    },
    subject_teacher: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    subject: {
      type: String,
      required: true,
    },
    student_absentees: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
    ],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("AttendanceSlot", AttendanceSlotSchema);
