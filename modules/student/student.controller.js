const { sendSuccess } = require("../../utils/response");
const StudentModel = require("./student.model");

class Controller {
  Model = StudentModel;

  async getMyClassrooms(req, res) {
    const { _id: userID } = req.user;
    const student = await this.Model.findOne({ user: userID }).populate(
      "classrooms"
    );
    return sendSuccess(res, student.classrooms);
  }
}

module.exports = new Controller();
