const mongoose = require("mongoose");

const transformFields = [
  "id",
  "userID",
  "classrooms",
  "createdAt",
  "updatedAt",
];

const StudentSchema = new mongoose.Schema(
  {
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    classrooms: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Classroom",
      },
    ],
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

function autoPopulate(next) {
  this.populate("userID");
  next();
}
StudentSchema.pre("find", autoPopulate);
StudentSchema.pre("findOne", autoPopulate);

module.exports = mongoose.model("Student", StudentSchema);
