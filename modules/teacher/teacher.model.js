const mongoose = require("mongoose");

const transformFields = [
  "id",
  "userID",
  "name",
  "institute",
  "createdAt",
  "updatedAt",
];

const TeacherSchema = new mongoose.Schema(
  {
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    name: {
      type: String,
    },
    institute: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Institute",
    },
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

function autoPopulate(next) {
  this.populate("userID");
  next();
}
TeacherSchema.pre("find", autoPopulate);
TeacherSchema.pre("findOne", autoPopulate);

module.exports = mongoose.model("Teacher", TeacherSchema);
