const { roles } = require("../../config/roles");
const { sendSuccess, sendError } = require("../../utils/response");
const Institute = require("../institutes/institute.model");
const Classroom = require("../classroom/classroom.model");
const Teacher = require("./teacher.model");
const JoinRequest = require("../JoinRequest/JoinRequest.model");
const { types } = require("../../config/requestType");

class Controller {
  async joinInstitute(req, res, next) {
    const { _id: userID, role: userRole } = req.user;

    if (userRole !== roles[2]) {
      return sendError(next, "User has no access", 401);
    }

    Teacher.updateOne(
      { userID: userID },
      { $set: { institute: req.body.instituteID } },
      function (err, result) {
        if (err) {
          return sendError(next, err);
        }
        return sendSuccess(res, result);
      }
    );
  }

  async joinInstituteWithCode(req, res, next) {
    const { _id: userID, role: userRole } = req.user;
    const { code } = req.body;

    if (userRole !== roles[2]) {
      return sendError(next, "Unauthorized", 401);
    }

    const institute = await Institute.findOne({ institute_code: code })
      .lean()
      .exec();
    if (!institute) {
      return sendError(next, "Invalid code", 400);
    }

    const teacher = await Teacher.findOne({ userID }).lean().exec();
    if (teacher.institute) {
      return sendError(next, "Teacher has already joined an institute", 400);
    }

    try {
      const newReq = new JoinRequest({
        type: "INSTITUTE",
        for: institute._id,
        by: userID,
      });
      await newReq.save();
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, { message: "Institute join request created" });
  }

  // approve req by principal
  async joinInstituteRequest(req, res, next) {
    const { _id: userID, role: userRole } = req.user;

    if (userRole !== roles[2]) {
      return sendError(next, "User has no access", 401);
    }

    const teacher = await Teacher.findOne({ userID }).lean().exec();
    if (teacher.institute) {
      return sendError(next, "Teacher has already joined an institute", 400);
    }

    if (!(await Institute.findById(req.body.instituteID))) {
      return sendError(next, "Institute does not exist");
    }

    const newReq = new JoinRequest({
      type: types[0],
      for: req.body.instituteID,
      by: userID,
    });
    newReq
      .save()
      .then((joinRequest) => sendSuccess(res, joinRequest))
      .catch((err) => sendError(next, err));
  }

  async getClassrooms(req, res) {
    const { _id: userID } = req.user;
    const entities = await Classroom.find({ teacher: userID });

    return sendSuccess(res, entities);
  }
}

module.exports = new Controller();
