const { Router } = require("express");
const { checkAuth } = require("../../middlewares/authorization");
const controller = require("./teacher.controller");
const router = Router();

router
  .route("/create_institute_join_request")
  .post(checkAuth, controller.joinInstituteRequest);

router.route("/join_institute").post(checkAuth, controller.joinInstitute);

router
  .route("/join_institute_code")
  .post(checkAuth, controller.joinInstituteWithCode);

router.route("/get_classrooms").get(checkAuth, controller.getClassrooms);

module.exports = router;
