const mongoose = require("mongoose");

const transformFields = [
  "id",
  "institute_code",
  "name",
  "address",
  "phone_number",
  "institute_docs",
  "logo",
  "principal",
  "createdAt",
  "updatedAt",
];

const InstituteSchema = new mongoose.Schema(
  {
    institute_code: {
      type: String,
    },
    name: {
      type: String,
      required: "Name is required",
    },
    address: {
      type: String,
      required: "Address is required",
    },
    phone_number: {
      type: String,
      required: "Phone number is required",
    },
    principal: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    logo: Object,
    institute_docs: [
      {
        type: String,
      },
    ],
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

InstituteSchema.statics.generateCode = function () {
  return Math.random().toString(36).slice(2, 10);
};

module.exports = mongoose.model("Institute", InstituteSchema);
