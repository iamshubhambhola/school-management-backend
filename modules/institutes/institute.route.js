const { Router } = require("express");
const { checkAuth } = require("../../middlewares/authorization");
const { s3Upload } = require("../../middlewares/upload");
const controller = require("./institute.controller");
const router = Router();

router.route("/create_institute").post(checkAuth, controller.createInstitute);
router
  .route("/update_logo")
  .post(checkAuth, s3Upload.single("file"), controller.uploadLogo);
router.route("/get_institutes").get(checkAuth, controller.getInstitutes);
router.route("/get_all_institutes").get(controller.getAllInstitutes);
router.route("/get_teachers_of_institute").get(controller.getInstituteTeachers);

router.route("/:id").put(checkAuth, controller.update);

module.exports = router;
