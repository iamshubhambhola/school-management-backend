const { roles } = require("../../config/roles");
const { sendSuccess, sendError } = require("../../utils/response");
const { getFields } = require("../../utils/upload");
const Teacher = require("../teacher/teacher.model");
const Institute = require("./institute.model");

class Controller {
  async createInstitute(req, res, next) {
    const { _id: userID } = req.user;
    const newInstitute = new Institute({
      name: req.body.name,
      address: req.body.address,
      phone_number: req.body.phone_number,
      principal: userID,
      institute_code: Institute.generateCode(),
    });
    newInstitute
      .save()
      .then((institute) => sendSuccess(res, institute))
      .catch((err) => sendError(next, err));
  }

  async getInstitutes(req, res, next) {
    const { _id: userID, role } = req.user;
    if (role !== roles[1]) {
      return sendError(next, "User has no access", 401);
    }

    Institute.find({ principal: userID })
      .sort({ createdAt: -1 })
      .then((institutes) => {
        sendSuccess(res, institutes);
      })
      .catch((err) => sendError(next, err));
  }

  async getAllInstitutes(req, res, next) {
    Institute.find()
      .sort({ createdAt: -1 })
      .then((institutes) => {
        sendSuccess(res, institutes);
      })
      .catch((err) => sendError(next, err));
  }

  async getInstituteTeachers(req, res, next) {
    const { instituteID } = req.query;

    Teacher.find({ institute: instituteID })
      .populate("userID")
      .then((teachers) => {
        return sendSuccess(res, teachers);
      })
      .catch((err) => sendError(next, err));
  }

  async uploadLogo(req, res, next) {
    const { role } = req.user;

    if (role !== roles[1]) {
      return sendError(next, "User has no access", 401);
    }
    const { instituteID } = req.body;

    Institute.findByIdAndUpdate(
      instituteID,
      { logo: getFields(req.file) },
      { new: true }
    )
      .then((institute) => {
        sendSuccess(res, institute);
      })
      .catch((err) => sendError(next, err));
  }

  async update(req, res, next) {
    const { id: instituteID } = req.params;

    let updatedInstitute;
    try {
      updatedInstitute = await Institute.findByIdAndUpdate(
        instituteID,
        req.body,
        { new: true }
      );
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, updatedInstitute);
  }
}

//upload central route
//response req.file

module.exports = new Controller();
