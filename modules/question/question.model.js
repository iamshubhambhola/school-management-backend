const mongoose = require("mongoose");
const UploadFileSchema = require("../common/uploadFile.schema");

const transformFields = [
  "id",
  "title",
  "image",
  "quesType",
  "options",
  "marks",
  "createdAt",
  "updatedAt",
];

const QuestionSchema = new mongoose.Schema(
  {
    homework: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Homework",
    },
    title: {
      type: String,
      required: "Title is required",
    },
    image: {
      type: UploadFileSchema,
    },
    quesType: {
      type: String,
      enum: ["MCQ", "INT"],
      default: "MCQ",
    },
    marks: {
      type: Number,
      default: 0,
    },
    options: [
      {
        text: {
          type: String,
          required: true,
        },
        isCorrect: {
          type: Boolean,
          required: true,
          default: false,
        },
      },
    ],
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

module.exports = mongoose.model("Question", QuestionSchema);
