const mongoose = require("mongoose");
const { jsonTransform } = require("../../utils/db");
const TTSlotSchema = new mongoose.Schema(
  {
    day: {
      type: Number,
      min: 0,
      max: 6,
      required: true,
    },
    time: {
      start: {
        type: Number,
        required: true,
      },
      end: {
        type: Number,
        required: true,
      },
    },
    subject: {
      type: String,
      required: true,
    },
    teacher: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
  },
  {
    _id: false,
  }
);

const transformFields = ["id", "days"];

const TimetableSchema = new mongoose.Schema(
  {
    days: [
      {
        name: {
          type: Number,
          min: 0,
          max: 6,
          required: true,
        },
        slots: [
          {
            type: TTSlotSchema,
          },
        ],
      },
    ],
  },
  {
    timestamps: true,
    toJSON: {
      transform: (doc) => jsonTransform(doc, transformFields),
    },
  }
);

module.exports = mongoose.model("Timetable", TimetableSchema);
