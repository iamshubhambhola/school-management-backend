const { Router } = require("express");
const { checkAPIAuth } = require("../../middlewares/authorization");
const { s3Upload } = require("../../middlewares/upload");
const controller = require("./upload.controller");
const router = Router();

router.post("/", checkAPIAuth, s3Upload.single("file"), controller.main);

module.exports = router;
