const { sendError, sendSuccess } = require("../../utils/response");
const { getFields } = require("../../utils/upload");

class Controller {
  async main(req, res, next) {
    if (!req.file) {
      return sendError(next, "Payload not found", 400);
    }

    sendSuccess(res, getFields(req.file));
  }
}

module.exports = new Controller();
