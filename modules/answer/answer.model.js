const mongoose = require("mongoose");

const transformFields = [
  "id",
  "question",
  "answers", 
  "createdAt",
  "updatedAt",
];

const AnswerSchema = new mongoose.Schema( // answered by student
  {
    question: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Question", 
    }, 
    answers: [
        {
            text: {
                type: String,
                required: true
            }, 
        }
    ]
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

module.exports = mongoose.model("Answer", AnswerSchema);
