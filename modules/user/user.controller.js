const { roles } = require("../../config/roles");
const { getUserModel } = require("../../utils/db");
const { sendSuccess, sendError } = require("../../utils/response");
const UserModel = require("./user.model");

class Controller {
  async selectRoleAndCreateUser(req, res, next) {
    const { _id: userID } = req.user;

    let currentUser = await UserModel.findById(userID);
    if (!currentUser) {
      return sendError(next, "User does not exist", 400);
    }

    if (currentUser.role !== roles[0]) {
      return sendError(next, "User role has already been defined", 401);
    }

    let newUserType;
    try {
      newUserType = await getUserModel(req.body.role).create({
        userID: currentUser._id,
      });
      currentUser = await UserModel.findByIdAndUpdate(userID, req.body, {
        new: true,
      });
      await currentUser.save();
    } catch (err) {
      return next(err);
    }

    return sendSuccess(res, { account: currentUser, details: newUserType });
  }
  /*
  invite student
  */
  //

  async me(req, res, next) {
    const { _id: userid } = req.user;
    const currentUser = await UserModel.findById(userid);
    if (!currentUser) {
      return sendError(next, "User does not exist", 400);
    }

    let entity = getUserModel(currentUser.role).find({ userID: userid });
    if (currentUser.role === roles[2] || currentUser.role === roles[3]) {
      entity.populate("institute");
    }
    entity = await entity.exec();

    return sendSuccess(res, { user: entity, currentUser });
  }

  async uploadImage(req, res) {
    sendSuccess(res, "Success");
  }

  async update(req, res, next) {
    const { _id: userID } = req.user;
    let updatedUser;
    try {
      updatedUser = await UserModel.findByIdAndUpdate(userID, req.body, {
        new: true,
      });
    } catch (err) {
      return next(err);
    }
    return sendSuccess(res, updatedUser);
  }
}

module.exports = new Controller();
