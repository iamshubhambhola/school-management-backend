const { Router } = require("express");
const { checkAuth } = require("../../middlewares/authorization");
const { s3Upload } = require("../../middlewares/upload");
const requestValidator = require("../../middlewares/validator");
const validation = require("../../validations/user.validation");
const controller = require("./user.controller");
const router = Router();

router.route("/").put(checkAuth, controller.update);
router
  .route("/select_user_role")
  .post(checkAuth, controller.selectRoleAndCreateUser);
router.route("/me").get(checkAuth, controller.me);
router
  .route("/upload_image")
  .post(s3Upload.single("image"), controller.uploadImage);

module.exports = router;
