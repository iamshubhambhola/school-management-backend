const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const { roles } = require("../../config/roles");
const UploadFileSchema = require("../common/uploadFile.schema");

const transformFields = [
  "id",
  "role",
  "createdAt",
  "blocked",
  "active",
  "confirmed",
  "phone_number",
  "name",
  "image",
];

const UserSchema = new mongoose.Schema(
  {
    phone_number: {
      type: String,
      maxlength: 12,
      minlength: 12,
    },
    role: {
      type: String,
      default: roles[0],
      enum: roles,
    },
    blocked: {
      type: Boolean,
      default: false,
    },
    confirmed: {
      type: Boolean,
      default: true,
    },
    otp: Number,
    name: {
      type: String,
    },
    image: {
      type: UploadFileSchema,
    },
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

UserSchema.pre("save", async function (next) {
  try {
    if (!this.isModified("password")) {
      return next();
    }

    this.password = await bcrypt.hash(this.password, 10);
    return next();
  } catch (err) {
    return next(err);
  }
});

UserSchema.methods = {
  transform() {
    const transformed = {};

    transformFields.forEach((field) => {
      transformed[field] = this[field];
    });

    return transformed;
  },

  passwordMatches(password) {
    return bcrypt.compareSync(password, this.password);
  },
};

module.exports = mongoose.model("User", UserSchema);
