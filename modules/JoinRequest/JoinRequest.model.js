const mongoose = require("mongoose");
const { types } = require("../../config/requestType");

const transformFields = ["id", "type", "for", "by", "createdAt", "updatedAt"];

const JoinRequestSchema = new mongoose.Schema(
  {
    type: {
      type: String,
      default: types[0],
      enum: types,
    },
    for: {
      type: String, //institute/class_id
    },
    by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc) {
        const transformed = {};

        transformFields.forEach((field) => {
          transformed[field] = doc[field];
        });

        return transformed;
      },
    },
  }
);

module.exports = mongoose.model("JoinRequest", JoinRequestSchema);
