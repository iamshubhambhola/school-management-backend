const UserModel = require("../user/user.model");
const { generateJWT } = require("../../utils/jwt");
const { generateOTP } = require("../../utils/auth");
const { sendSuccess, sendError } = require("../../utils/response");
const { getUserModel } = require("../../utils/db");
const { roles } = require("../../config/roles");
const message_bird_api_key = process.env.MESSAGE_BIRD_API_KEY;

var messagebird = require("messagebird")(message_bird_api_key);

class Controller {
  async sendOTP(req, res, next) {
    const { phone_number } = req.body;

    let otp = generateOTP();

    var params = {
      originator: "TestMessage",
      recipients: ["+" + phone_number],
      body: `OTP is ${otp}`,
    };
    messagebird.messages.create(params, function (err, response) {
      if (err) {
        return console.log(err);
      }
      console.log(response);
    });

    try {
      await UserModel.findOneAndUpdate(
        {
          phone_number,
        },
        {
          phone_number,
          otp,
        },
        { upsert: true }
      );
    } catch (err) {
      return next(err);
    }

    // send OTP through a service

    return sendSuccess(res, {
      message: "OTP has been sent!",
      otp,
    });
  }

  async verifyOTP(req, res, next) {
    const { phone_number, otp } = req.body;

    let user;
    try {
      user = await UserModel.findOne({ phone_number });
      if (!user) {
        return sendError(next, "User not found", 404);
      }
    } catch (err) {
      return next(err);
    }

    if (otp != parseInt(user.otp)) {
      return sendError(next, "Incorrect OTP", 401);
    }

    user.confirmed = true;
    user.otp = undefined;

    try {
      await user.save();
    } catch (err) {
      return next(err);
    }

    let token;
    let details;
    if (user.role !== roles[0]) {
      details = await getUserModel(user.role).findOne({ userID: user._id });
    }

    token = generateJWT({ id: user._id, role: user.role });

    return sendSuccess(res, {
      token,
      account: user,
      details,
    });
  }
}

module.exports = new Controller();
