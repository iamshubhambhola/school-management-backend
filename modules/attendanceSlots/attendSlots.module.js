const mongoose = require("mongoose"); 
const AttendSlotsSchema = new mongoose.Schema(
  { 
    day: {
      type: Date,  
      required: true,
      required: 'Day is required'
    },
    time:{
        type:Date,
        require:true,
        required:'Time is required'
    },
    class:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Classroom',
        require:true,
        required:'Class is required'
    },
    subject:{
        type:String,
    },
    student_absentees:[
        {
            type:mongoose.Schema.Types.ObjectId,
            ref:'User',
        }
    ]
  },
  {
    timestamps: true
  }
); 

module.exports = mongoose.model("AttendanceSlot", AttendSlotsSchema);
