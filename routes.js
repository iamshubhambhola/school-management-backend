const { Router } = require("express");
const router = Router();

const authRouter = require("./modules/auth/auth.route");
const userRouter = require("./modules/user/user.route");
const announcementRouter = require("./modules/announcements/announcement.route");
const classroomRouter = require("./modules/classroom/classroom.route");
const instituteRouter = require("./modules/institutes/institute.route");
const teacherRouter = require("./modules/teacher/teacher.route");
const principalRouter = require("./modules/principal/principal.route");
const homeworkRouter = require("./modules/homework/homework.route");
const uploadRouter = require("./modules/upload/upload.route");

router.use("/auth", authRouter);
router.use("/user", userRouter);
router.use("/announcement", announcementRouter);
router.use("/classroom", classroomRouter);
router.use("/institute", instituteRouter);
router.use("/teacher", teacherRouter);
router.use("/principal", principalRouter);
router.use("/homework", homeworkRouter);
router.use("/upload", uploadRouter);

module.exports = router;
